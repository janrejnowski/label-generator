#Labels generator for SAM Totem.com.pl
> System Asset Management for company [TOTEM.COM.PL](https://www.totem.com.pl)

This is a package made exclusively for private project for asset management in IT Operations.
It contains form and views needed to create a label.

It is built on [Laravel 5.5](http://laravel.com).

-----

## General System Requirements

| Requirement | Notes |
| --- | --- |
| PHP | >7.1.0 |
| MySQL | >5.7 |

## Quick Installation

```bash
$ composer require janrejnowski/label-generator:"~1.0"
```

#### Service Provider (Optional on Laravel 5.5)
Register provider on your `config/app.php` file.

```php
'providers' => [
    ...,
    JanRejnowski\LabelGenerator\LabelServiceProvider::class,
]
```

#### Configuration

```bash
$ php artisan vendor:publish --provider=JanRejnowski\LabelGenerator\LabelServiceProvider
```

##### Warning !
After update package don't forget to update **Views** by publish or manually make changes.

```bash
$ php artisan vendor:publish --tag=label-generator-views
```

## Translations

Package supports multi languages. Currently available is **pl** and **en**.
If you'd like to translate package into a language that we don't offer, simply let us know via email.

-----

## Author
* **Jan Rejnowski** - Junior programmer

## Release History
* Initial
    * First Initialize with form and label views.