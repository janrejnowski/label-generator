<?php

Route::group([ 'middleware' => ['web', 'auth']], function (){

    Route::group(['prefix' => 'orders', 'middleware' => ['permission:job-ticket|delivery-statement|label-generator']], function() {

        Route::group(['prefix' => 'label', 'middleware' => ['permission:label-generator']], function() {
            Route::get('/','JanRejnowski\LabelGenerator\Controllers\LabelController@index')->name('label-generator.index');
            Route::post('/display', 'JanRejnowski\LabelGenerator\Controllers\LabelController@display')->name('label-generator.display');

            Route::group(['prefix' => 'pdf'], function (){
                Route::post('/delivery', 'JanRejnowski\LabelGenerator\Controllers\LabelController@downloadDeliveryPdf')->name('label-generator.pdf.delivery');
                Route::post('/palette', 'JanRejnowski\LabelGenerator\Controllers\LabelController@downloadPalettePdf')->name('label-generator.pdf.palette');
            });

            Route::post('/email','JanRejnowski\LabelGenerator\Controllers\LabelController@email')->name('label-generator.email');
        });

    });
});
