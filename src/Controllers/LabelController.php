<?php

namespace JanRejnowski\LabelGenerator\Controllers;

use Illuminate\Http\Request;
use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Support\Facades\Notification;
use JanRejnowski\LabelGenerator\LabelServiceProvider;
use JanRejnowski\LabelGenerator\Notifications\PdfCreated;
use App\Http\Controllers\Controller;

class LabelController extends Controller
{
    public function index(LabelServiceProvider $labelServiceProvider) : \Illuminate\View\View
    {
        return view('label-generator::label.create')->with([
            'languages' => $labelServiceProvider->getLanguagesArray(),
            'locale' => app()->getLocale()
        ]);
    }

    public function downloadDeliveryPdf(Request $request) : \Illuminate\Http\Response
    {
        app()->setLocale($request->language);
        /** @var $pdf PdfWrapper  */
        $pdf = app()->make(PdfWrapper::class);
        $pdf->loadView('label-generator::label.pdf.delivery_note', [
            'customer_name' => $request->input('delivery.customer.name'),
            'customer_address' => $request->input('delivery.customer.address'),
            'customer_city' => $request->input('delivery.customer.city'),
            'customer_post_code' => $request->input('delivery.customer.post_code'),
            'customer_country' => $request->input('delivery.customer.country'),
            'shipping_name' => $request->input('delivery.shipping.name'),
            'shipping_address' => $request->input('delivery.shipping.address'),
            'shipping_city' => $request->input('delivery.shipping.city'),
            'shipping_post_code' => $request->input('delivery.shipping.post_code'),
            'shipping_country' => $request->input('delivery.shipping.country'),
            'customer' => $request->input('delivery.customer_name'),
            'order_name' => $request->input('delivery.order_name'),
            'isbn' => $request->input('delivery.isbn'),
            'copies_number' => $request->input('delivery.copies_number'),
            'cartons_number' => $request->input('delivery.cartons_number'),
            'total_copies' => $request->input('delivery.total_copies'),
            'total_pallets' => $request->input('delivery.total_pallets'),
            'palette_dimensions' => $request->input('delivery.palette_dimensions'),
            'net_weight' => $request->input('delivery.net_weight'),
            'gross_weight' => $request->input('delivery.gross_weight'),
            'date' => $request->input('delivery.date'),
        ]);
        return $pdf->download(LabelServiceProvider::setPdfName('delivery-note',$request->input('order_number')));
    }

    public function downloadPalettePdf(Request $request) : \Illuminate\Http\Response
    {
        app()->setLocale($request->language);
        /** @var $pdf PdfWrapper  */
        $pdf = app()->make(PdfWrapper::class);
        $pdf->loadView('label-generator::label.pdf.palette_label', [
            'customer' => $request->input('palette.customer.name'),
            'title' => $request->input('palette.title'),
            'isbn' => $request->input('palette.isbn'),
            'price' => $request->input('palette.price'),
            'cartons_number' => $request->input('palette.cartons_number'),
            'total_quantity'=> $request->input('palette.total_quantity'),
            'palette_shipping_name' => $request->input('palette.shipping.name'),
            'palette_shipping_address' => $request->input('palette.shipping.address'),
            'palette_shipping_city' => $request->input('palette.shipping.city'),
            'palette_shipping_post_code' => $request->input('palette.shipping.post_code'),
            'palette_shipping_country' => $request->input('palette.shipping.country'),
            'palette_telephone' => $request->input('palette.telephone'),
            'palette_fax' => $request->input('palette.fax'),
            'palette_email' => $request->input('palette.email')
        ]);
        return $pdf->download(LabelServiceProvider::setPdfName('palette-label',$request->input('order_number')));
    }

    public function email(Request $request) : \Illuminate\Routing\Redirector
    {
        app()->setLocale($request->language);
        /** @var $pdf PdfWrapper  */
        $pdf = app()->make(PdfWrapper::class);
        $pdf->loadView('label-generator::label.pdf.delivery_note', [
            'customer_name' => $request->input('delivery.customer.name'),
            'customer_address' => $request->input('delivery.customer.address'),
            'customer_city' => $request->input('delivery.customer.city'),
            'customer_post_code' => $request->input('delivery.customer.post_code'),
            'customer_country' => $request->input('delivery.customer.country'),
            'shipping_name' => $request->input('delivery.shipping.name'),
            'shipping_address' => $request->input('delivery.shipping.address'),
            'shipping_city' => $request->input('delivery.shipping.city'),
            'shipping_post_code' => $request->input('delivery.shipping.post_code'),
            'shipping_country' => $request->input('delivery.shipping.country'),
            'customer' => $request->input('delivery.customer_name'),
            'order_name' => $request->input('delivery.order_name'),
            'isbn' => $request->input('delivery.isbn'),
            'copies_number' => $request->input('delivery.copies_number'),
            'cartons_number' => $request->input('delivery.cartons_number'),
            'total_copies' => $request->input('delivery.total_copies'),
            'total_pallets' => $request->input('delivery.total_pallets'),
            'palette_dimensions' => $request->input('delivery.palette_dimensions'),
            'net_weight' => $request->input('delivery.net_weight'),
            'gross_weight' => $request->input('delivery.gross_weight'),
            'date' => $request->input('delivery.date'),
        ]);

        try{
            Notification::route('mail', $request->input('pdf_email'))->notify(new PdfCreated($pdf, $request));
            return redirect()->route('label-generator.index')
                ->with('success', __('Email with Delivery Note for order :number sent successfully.', ['number' => $request->order_number]));
        } catch (\Exception $e) {
            return redirect()->route('label-generator.index')->with('error', $e->getMessage());
        }
    }
}