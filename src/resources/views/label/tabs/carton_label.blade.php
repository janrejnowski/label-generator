<div class="box-body form-horizontal">
    <div class="form-group">
        <div class="col-xs-3 text-uppercase control-label">
            <b data-language="Publisher"></b>
        </div>
        <div class="col-xs-9">
            <span class="print-no-border form-control print-height-auto" data-input="customer[name]" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 control-label">
            <b>ISBN</b>
        </div>
        <div class="col-xs-9">
            <span class="print-no-border form-control" data-input="isbn" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 text-uppercase control-label">
            <b data-language="Author"></b>
        </div>
        <div class="col-xs-9">
            <span class="print-no-border form-control" data-input="author" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 text-uppercase control-label">
            <b data-language="Title"></b>
        </div>
        <div class="col-xs-9">
            <span class="print-no-border form-control print-height-auto" data-input="order_name" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 text-uppercase control-label">
            <b data-language="No"></b>
        </div>
        <div class="col-xs-9">
            <span class="print-no-border form-control" data-input="order_number" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-uppercase control-label">
            <b data-language="Pbk"></b>
        </div>
        <div class="col-xs-3">
            <span data-input="weight" contenteditable="true"></span>&nbsp;<span data-language="kgs"></span>
        </div>
        <div class="col-xs-4">
            <span data-input="price" contenteditable="true"></span>&nbsp;<span data-input="price_currency"></span>
        </div>
        <div class="col-xs-3">
            <span data-input="copies_quantity" contenteditable="true"></span>&nbsp;<span data-language="cps"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 text-uppercase control-label">
            <b data-language="Carton dimensions"></b>
        </div>
        <div class="col-xs-6">
            <span data-input="dimensions" contenteditable="true"></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6 col-xs-offset-3">
            <img class="center-block" id="barcode" src=''>
            <i class="fa fa-close close"></i>
        </div>
    </div>
</div>