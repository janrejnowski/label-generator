<div id="delivery" class="box-body form-horizontal">
    <div class="print-no-display">
        <div class="form-group">
            <label for="address" class="col-xs-3 col-xs-offset-2 control-label text-uppercase">{{__('Choose address')}}</label>
            <div class="col-xs-4">
                <select name="address" id="address" class="form-control address">
                    <option value=""></option>
                </select>
            </div>
        </div>
    </div>
    <hr class="print-no-display">
    <div class="row">

        <div class="col-lg-6">
            <div class="form-group">
                <div class="col-xs-12">
                    <b data-language="Customer/Publisher:"></b>
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer][name]" class="col-xs-2 control-label" data-language="Name"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[customer][name]" name="delivery[customer][name]" class="form-control print-no-border" data-input="customer[name]">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer][address]" class="col-xs-2 control-label" data-language="Address"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[customer][address]" name="delivery[customer][address]" class="form-control print-no-border" data-input="customer[address]">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer][city]" class="col-xs-2 control-label" data-language="City"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[customer][city]" name="delivery[customer][city]" class="form-control print-no-border" data-input="customer[city]">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer][post_code]" class="col-xs-2 control-label" data-language="Postal code"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[customer][post_code]" name="delivery[customer][post_code]" class="form-control print-no-border" data-input="customer[post_code]">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer][country]" class="col-xs-2 control-label" data-language="Country"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[customer][country]" name="delivery[customer][country]" class="form-control print-no-border" data-input="customer[country]">
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <div class="col-xs-12">
                    <b data-language="Shipping Address:"></b>
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[shipping][name]" class="col-xs-2 control-label" data-language="Name"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[shipping][name]" name="delivery[shipping][name]" class="form-control print-no-border" data-input="shipping_name">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[shipping][address]" class="col-xs-2 control-label" data-language="Address"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[shipping][address]" name="delivery[shipping][address]" class="form-control print-no-border" data-input="shipping_address">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[shipping][city]" class="col-xs-2 control-label" data-language="City"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[shipping][city]" name="delivery[shipping][city]" class="form-control print-no-border" data-input="shipping_city">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[shipping][post_code]" class="col-xs-2 control-label" data-language="Postal code"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[shipping][post_code]" name="delivery[shipping][post_code]" class="form-control print-no-border" data-input="shipping_post_code">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[shipping][country]" class="col-xs-2 control-label" data-language="Country"></label>
                <div class="col-xs-9">
                    <input type="text" id="delivery[shipping][country]" name="delivery[shipping][country]" class="form-control print-no-border" data-input="shipping_country">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group"></div>
            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-4 text-uppercase">
                    <b data-language="Delivery note"></b>
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[customer_name]" class="col-xs-4 control-label" data-language="Publisher"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[customer_name]" name="delivery[customer_name]" class="form-control print-no-border" data-input="customer[name]">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[order_name]" class="col-xs-4 control-label" data-language="Title"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[order_name]" name="delivery[order_name]" class="form-control print-no-border" data-input="order_name">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[isbn]" class="col-xs-4 control-label">ISBN</label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[isbn]" name="delivery[isbn]" class="form-control print-no-border" data-input="isbn">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[copies_number]" class="col-xs-4 control-label" data-language="Number of copies per carton"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[copies_number]" name="delivery[copies_number]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[cartons_number]" class="col-xs-4 control-label" data-language="Total number of cartons"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[cartons_number]" name="delivery[cartons_number]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[total_copies]" class="col-xs-4 control-label" data-language="Total number of copies"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[total_copies]" name="delivery[total_copies]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[total_pallets]" class="col-xs-4 control-label" data-language="Total number of pallets"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[total_pallets]" name="delivery[total_pallets]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[palette_dimensions]" class="col-xs-4 control-label" data-language="Palette dimensions"></label>
                <div class="col-xs-8">
                    <select id="delivery[palette_dimensions]" name="delivery[palette_dimensions]" class="form-control selectizer">
                        <option value=""></option>
                        <option value="1200x800">1200 x 800</option>
                        <option value="1200x1000">1200 x 1000</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[net_weight]" class="col-xs-4 control-label" data-language="Net weight"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[net_weight]" name="delivery[net_weight]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="delivery[gross_weight]" class="col-xs-4 control-label" data-language="Gross weight"></label>
                <div class="col-xs-8">
                    <input type="text" id="delivery[gross_weight]" name="delivery[gross_weight]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4 control-label">
                    <b data-language="Loading/dispatch date"></b>
                </div>
                <div class="col-xs-3 col-xs-offset-3">
                    <div class="input-group date" id="filter-created_at">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control input-sm print-no-border" id="delivery[date]" name="delivery[date]" title="" data-rel="month-picker">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-sm btn-default print-no-display" data-filters="clear">
                            <i class="fa fa-times"></i>
                        </button>
                        </span>
                    </div>
                </div>
                </div>
            <div class="form-group">
                <div class="col-xs-4 control-label">
                    <b data-language="Country of origin"></b>
                </div>
                <div class="col-xs-3 col-xs-offset-3 text-uppercase" data-language="Poland">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-9 col-xs-offset-3">
                    <b data-language="Please return one signed copy to the driver"></b>
                </div>
            </div>
        </div>
    </div>
</div>