<div class="box-body print-zoom form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="palette[customer][name]" class="col-xs-4 control-label text-uppercase" data-language="Publisher"></label>
                <div class="col-md-8 col-xs-12">
                    <input type="text" id="palette[customer][name]" name="palette[customer][name]" class="form-control print-no-border" data-input="customer[name]">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[title]" class="col-xs-4 control-label text-uppercase" data-language="Title"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[title]" name="palette[title]" class="form-control print-no-border" data-input="order_name">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[isbn]" class="col-xs-4 control-label text-uppercase">ISBN</label>
                <div class="col-xs-8">
                    <input type="text" id="palette[isbn]" name="palette[isbn]" class="form-control print-no-border" data-input="isbn">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[price]" class="col-xs-4 control-label text-uppercase" data-language="Retail price"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[price]" name="palette[price]" class="form-control print-no-border" data-input="price">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[cartons_number]" class="col-xs-4 control-label text-uppercase" data-language="Number of cartons"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[cartons_number]" name="palette[cartons_number]" class="form-control print-no-border">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[total_quantity]" class="col-xs-4 control-label text-uppercase" data-language="Total quantity"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[total_quantity]" name="palette[total_quantity]" class="form-control print-no-border">
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-6 text-uppercase">
            <b data-language="Address of delivery"></b>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group print-no-display control-label">
                <label for="address" class="col-xs-3 col-xs-offset-2 control-label text-uppercase">{{__('Choose address')}}</label>
                <div class="col-xs-4">
                    <select name="address" id="address" class="form-control address">
                        <option value=""></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="palette[shipping][name]" class="col-xs-4 control-label text-uppercase" data-language="Name"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[shipping][name]" name="palette[shipping][name]" class="form-control print-no-border" data-input="shipping_name">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[shipping][address]" class="col-xs-4 control-label text-uppercase" data-language="Address"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[shipping][address]" name="palette[shipping][address]" class="form-control print-no-border" data-input="shipping_address">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[shipping][post_code]" class="col-xs-4 control-label text-uppercase" data-language="Postal code"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[shipping][post_code]" name="palette[shipping][post_code]" class="form-control print-no-border" data-input="shipping_post_code">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[shipping][city]" class="col-xs-4 control-label text-uppercase" data-language="City"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[shipping][city]" name="palette[shipping][city]" class="form-control print-no-border" data-input="shipping_city">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[shipping][country]" class="col-xs-4 control-label text-uppercase" data-language="Country"></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[shipping][country]" name="palette[shipping][country]" class="form-control print-no-border" data-input="shipping_country">
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="palette[telephone]" class="col-xs-4 control-label"><b>Tel:</b></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[telephone]" name="palette[telephone]" class="form-control print-no-border" data-input="telephone">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[fax]" class="col-xs-4 control-label"><b>Fax:</b></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[fax]" name="palette[fax]" class="form-control print-no-border" data-input="fax">
                </div>
            </div>
            <div class="form-group">
                <label for="palette[email]" class="col-xs-4 control-label"><b>Email:</b></label>
                <div class="col-xs-8">
                    <input type="text" id="palette[email]" name="palette[email]" class="form-control print-no-border" data-input="email">
                </div>
            </div>
        </div>
    </div>
</div>
