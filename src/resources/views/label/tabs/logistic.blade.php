<div id="logistic" class="box-body row">
    <div class="col-xs-12">
        <div class="form-group">
            <div class="col-xs-3 text-uppercase control-label"><b>{{ __('Title') }}</b></div>
            <div class="col-xs-9">
                <p class="form-control-static" data-input="order_name"></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-3 text-uppercase control-label"><b>{{ __('Order number') }}</b></div>
            <div class="col-xs-9">
                <p class="form-control-static" data-input="order_number"></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-3 text-uppercase control-label"><b>{{ __('Customer') }}</b></div>
            <div class="col-xs-9">
                <p class="form-control-static" data-input="customer[name]"></p>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr class="text-center">
                    <th colspan="4" class="bg-default text-center text-uppercase"><strong>{{ __('One unit weight') }}</strong></th>
                    <th colspan="4" class="bg-default text-center text-uppercase"><strong>{{ __('Total weight') }}</strong></th>
                </tr>
            </thead>
            <tbody>
                <tr class="text-center">
                    <td colspan="4">
                        <span data-input="mass"></span> g
                    </td>
                    <td colspan="4">
                        <span data-input="mass_total"></span> kg
                    </td>
                </tr>
            </tbody>
            <tbody class="hide" data-id="shipping_address">
                <tr>
                    <td class="bg-default text-uppercase"><strong>{{ __('No.') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Delivery method') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Supplier') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Package') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Setting quantity') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Packaging method') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Quantity') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Mass') }} (kg)</strong></td>
                </tr>
                <tr>
                    <td rowspan="3">
                        <span data-input="shipping_no"></span>
                    </td>
                    <td>
                        <span data-input="delivery_method"></span>
                    </td>
                    <td>
                        <span data-input="supplier"></span>
                    </td>
                    <td>
                        <span data-input="package"></span>
                    </td>
                    <td>
                        <span data-input="setting_quantity"></span>
                    </td>
                    <td>
                        <span data-input="packaging_method"></span>
                    </td>
                    <td>
                        <span data-input="quantity"></span>
                    </td>
                    <td>
                        <span data-input="mass"></span>
                    </td>
                </tr>
                <tr>
                    <td class="bg-default text-uppercase"><strong>{{ __('Recipient') }}</strong></td>
                    <td class="bg-default text-uppercase" colspan="2"><strong>{{ __('Address') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Planned shipping date') }}</strong></td>
                    <td class="bg-default text-uppercase"><strong>{{ __('Shipping date') }}</strong></td>
                    <td colspan="2" class="bg-default text-uppercase"><strong>{{ __('Legible signature') }}</strong></td>
                </tr>
                <tr>
                    <td>
                        <span data-input="recipient"></span>
                    </td>
                    <td colspan="2">
                        <span data-input="postal_code"></span>
                        <span data-input="city"></span>
                        <span data-input="street"></span>
                        <span data-input="building_number"></span>
                        <span data-input="suite_number"></span>
                    </td>
                    <td>
                        <span data-input="shipping_date"></span>
                    </td>
                    <td>
                        <span data-input="shipping_deadline"></span>
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td class="bg-default text-uppercase" colspan="8"><strong>{{ __('Notes') }}</strong></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <p data-input="description" data-enter=true></p>
                    </td>
                    <td colspan="4">
                        <p contenteditable="true"></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>