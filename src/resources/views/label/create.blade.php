@extends('label-generator::label.panel')

@section('title', __('Create new label'))

@section('content')
<div class="row">
    <section class="content-header">
        <h1>{{ __('Create new label') }}</h1>
        @component('layout.breadcrumb', [ 'data' => [ 'Label' => '' ] ])@endcomponent
    </section>
    <div class="col-xs-12">
        @component('layout.alert')
            @include('layout.errors')
        @endcomponent
        <div class="row">
            <form id="label-generator" class="form-horizontal" method="POST" action="">
                {{ csrf_field() }}
                <div class="col-xs-12 col-lg-offset-2 col-lg-8">
                    <div class="box box-default">
                        <ul class="nav nav-tabs hidden-print">
                            <li class="active"><a data-toggle="tab" href="#home">{{ __('Form') }}</a></li>
                            <li class="disabled"><a data-toggle="tab" href="#carton_label">{{ __('Carton label') }}</a></li>
                            <li class="disabled"><a data-toggle="tab" href="#palette_label">{{ __('Palette label') }}</a></li>
                            <li class="disabled"><a data-toggle="tab" href="#delivery_note">{{ __('Delivery note') }}</a></li>
                            <li class="disabled"><a data-toggle="tab" href="#logistic">{{ __('Spedition cards') }}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="box-body">
                                    @include('label-generator::label.forms.main_form')
                                    <hr>
                                    @include('label-generator::label.forms.address_form')
                                </div>
                            </div>
                            <div id="carton_label" class="tab-pane fade in">
                                @include('label-generator::label.tabs.carton_label')
                            </div>
                            <div id="palette_label" class="tab-pane fade in">
                                @include('label-generator::label.tabs.palette_label')
                            </div>
                            <div id="delivery_note" class="tab-pane fade in">
                                @include('label-generator::label.tabs.delivery_note')
                            </div>
                            <div id="logistic" class="tab-pane fade in">
                                @include('label-generator::label.tabs.logistic')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2" id="print-button">
                    <div class="form-group">
                        <button type="button" id="print" class="btn btn-default disabled" disabled>
                            <i class="fa fa-4x fa-fw fa-print"></i>
                        </button>
                    </div>
                </div>
                <div class="col-lg-2 print-no-display" id="pdf-button">
                    <div class="form-group">
                        <button type="submit" id="pdf" class="btn btn-primary disabled" disabled>
                            <i class="fa fa-4x fa-fw fa-file-pdf-o"></i>
                        </button>
                    </div>
                </div>
                <div class="col-lg-2 print-no-display" id="email-button">
                    <div class="form-group">
                        <button type="submit" id="email" formaction="{{ route('label-generator.email') }}" class="btn btn-primary disabled" disabled>
                            <i class="fa fa-4x fa-fw fa-envelope-o"></i>
                        </button>
                    </div>
                </div>
                <div class="col-lg-2 print-no-display"><b>{{__('Choose language')}}</b></div>
                <div class="col-lg-2 print-no-display">
                    <select class="btn btn-default dropdown-toggle disabled" id="language" name="language" title="{{ __('Choose language') }}" disabled>
                        <option value="pl">PL</option>
                        <option value="en">ENG</option>
                    </select>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@include('layout.datePicker.dialog-with-scripts', ['minViewMode' => 0])


@push('scripts')
    <script type="text/javascript" charset="utf-8" src="{{ asset('js/JsBarcode.ean-upc.min.js') }}"></script>
    <script type="text/javascript" charset="utf-8" src="{{ asset('js/selectize.min.js') }}"></script>
@endpush
@push('scripts-inline')
<script>
    $(function(){
        let languages = @json($languages),
            locale = '{{ $locale }}',
            $formBox = $('#value-box'),
            $addressSelect = $('.address'),
            $addressBox = $('#address-box'),
            addressPanel = '.address-box',
            $languageSelect = $('#language'),
            $customerBox = $('#customer-box'),
            $operatorMail = $('#pdf_email'),
            $barcode = $("#barcode"),
            $logistic = $('#logistic'),
            f = {
                labels : {
                    /**
                     * dodawanie nowego formularza na adres
                     */
                    addNewAddressForm: function () {
                        let $html = $formBox.find(addressPanel+'.hide'),
                            $clone = $html.clone(),
                            prev_id = $html.prev(addressPanel).attr('data-id'),
                            current_id = parseInt(typeof prev_id !== "undefined" ? prev_id : '0') + 1,
                            shipping_name = 'shipping[' + current_id + '][name]',
                            shipping_address = 'shipping[' + current_id + '][address]',
                            shipping_city = 'shipping[' + current_id + '][city]',
                            shipping_post_code = 'shipping[' + current_id + '][postal_code]',
                            shipping_country = 'shipping[' + current_id + '][country]',
                            telephone = 'shipping[' + current_id + '][phone_number]',
                            fax = 'shipping[' + current_id + '][fax]',
                            email = 'shipping[' + current_id + '][email]'
                        ;

                        $clone.attr('data-id', current_id);
                        $clone.find('label[for="shipping_name[0]"]').attr('for', shipping_name);
                        $clone.find('label[for="shipping_address[0]"]').attr('for', shipping_address);
                        $clone.find('label[for="shipping_city[0]"]').attr('for', shipping_city);
                        $clone.find('label[for="shipping_post_code[0]"]').attr('for', shipping_post_code);
                        $clone.find('label[for="shipping_country[0]"]').attr('for', shipping_country);
                        $clone.find('label[for="telephone[0]"]').attr('for', telephone);
                        $clone.find('label[for="fax[0]"]').attr('for', fax);
                        $clone.find('label[for="email[0]"]').attr('for', email);

                        $clone.find('input[id="shipping_name[0]"]').attr('id', shipping_name);
                        $clone.find('input[id="shipping_address[0]"]').attr('id', shipping_address);
                        $clone.find('input[id="shipping_city[0]"]').attr('id', shipping_city);
                        $clone.find('input[id="shipping_post_code[0]"]').attr('id', shipping_post_code);
                        $clone.find('input[id="shipping_country[0]"]').attr('id', shipping_country);
                        $clone.find('input[id="telephone[0]"]').attr('id', telephone);
                        $clone.find('input[id="fax[0]"]').attr('id', fax);
                        $clone.find('input[id="email[0]"]').attr('id', email);

                        $clone.find('input[name="shipping_name[0]"]').attr('name', shipping_name);
                        $clone.find('input[name="shipping_address[0]"]').attr('name', shipping_address);
                        $clone.find('input[name="shipping_city[0]"]').attr('name', shipping_city);
                        $clone.find('input[name="shipping_post_code[0]"]').attr('name', shipping_post_code);
                        $clone.find('input[name="shipping_country[0]"]').attr('name', shipping_country);
                        $clone.find('input[name="telephone[0]"]').attr('name', telephone);
                        $clone.find('input[name="fax[0]"]').attr('name', fax);
                        $clone.find('input[name="email[0]"]').attr('name', email);
                        $html.before($clone.removeClass('hide'));
                    },
                    barcode: {
                        displayCode(formCode){
                            if(formCode === null){
                                return;
                            }
                            let code = formCode.replace(/\s|-/g, ''),
                                count = formCode.length;
                            if (count < 12){
                                f.labels.barcode.displayIssn(code);
                            } else if (count >= 12){
                                f.labels.barcode.displayIsbn(code)
                            }
                        },
                        hide() {
                            $barcode.hide();
                            $barcode.next('.close').hide();
                        },
                        show() {
                            $barcode.show();
                            $barcode.next('.close').show();
                        },
                        displayIsbn(code){
                            try {
                                $barcode.JsBarcode(code);
                            } catch(err) {
                                $barcode.attr('src', '');
                            }
                        },
                        displayIssn(code){
                            return code;
                        },
                    },

                    /**
                     * dodaje option selecta na adresy
                     */
                    addAddressSelect(address){
                        $addressSelect.empty();
                        $addressSelect.append('<option></option>');
                        for ( let index = 0; index < address.length; index++){
                            $addressSelect.append($('<option>', {
                                value: index + 1,
                                text : address[index].name
                            }));
                        }
                    },

                    getFormAddresses(){
                        let result = [];
                        $formBox.find(addressPanel+':not(.hide)').each(function () {
                            let element = $(this);
                            result.push(
                                {
                                    'name': element.find('input[name="shipping['+element.attr('data-id')+'][name]"]').val()
                                }
                            )
                        });
                        return result;
                    },

                    clearAddressForm() {
                        $formBox.find(addressPanel+':not(.hide)').remove();
                    },

                    clearMainForm() {
                        $('#main-form').find('.form-group').find('input').val('');
                    },

                    /**
                     * pobiera adresy z Optimy i uzupełnia nimi formularze
                     */
                    getShippingAddress(response){

                        let databaseAddress = response.data.shipping;
                        for (let i = 0; i < databaseAddress.length; i++) {
                            let addressWithoutSuite = databaseAddress[i].street + ' ' + databaseAddress[i].building_number,
                                addressWithSuite = addressWithoutSuite + '/' + databaseAddress[i].suite_number,
                                addressInput = "input[name='shipping\[" + (i+1) + "]\[address]']",
                                suiteNumber = databaseAddress[i].suite_number;

                            f.labels.addNewAddressForm();
                            $addressBox.find(addressInput).val(addressWithoutSuite);
                            if (suiteNumber && suiteNumber.replace(/\s/g, '').length !== 0){
                                $addressBox.find(addressInput).val(addressWithSuite);
                            }

                            $addressBox.find("input[name='shipping\\[" + (i+1) + "]\\[postal_code]']").val(databaseAddress[i].postal_code);
                            $addressBox.find("input[name='shipping\\[" + (i+1) + "]\\[name]']").val(databaseAddress[i]['name']);
                            $addressBox.find("input[name='shipping\\[" + (i+1) + "]\\[city]']").val(databaseAddress[i].city);
                            $addressBox.find("input[name='shipping\\[" + (i+1) + "]\\[phone_number]']").val(databaseAddress[i].phone_number);
                            $addressBox.find("input[name='shipping\\[" + (i+1) + "]\\[country]']").val(databaseAddress[i].country);
                        }
                        f.labels.addAddressSelect(databaseAddress);
                    },

                    getCustomerAddress(response){
                        let databaseAddress = response.data.customer;
                        let addressWithoutSuite = databaseAddress.street + ' ' + databaseAddress.building_number,
                            addressWithSuite = addressWithoutSuite + '/' + databaseAddress.suite_number,
                            addressInput = "input[name='customer[address]']",
                            suiteNumber = databaseAddress.suite_number;

                        $customerBox.find(addressInput).val(addressWithoutSuite);
                        if (suiteNumber && suiteNumber.replace(/\s/g, '').length !== 0){
                            $customerBox.find(addressInput).val(addressWithSuite);
                        }

                        $customerBox.find("input[name='customer[name]']").val(databaseAddress['name']);
                        $customerBox.find("input[name='customer[post_code]']").val(databaseAddress.postal_code);
                        $customerBox.find("input[name='customer[country]']").val(databaseAddress.country);
                        $customerBox.find("input[name='customer[city]']").val(databaseAddress.city);
                    },

                    setDataInForm() {
                        let $inputs = $('#label-generator').serializeArray();
                        for (let i = 0; i < $inputs.length; i++) {
                            let $dataInput = $('[data-input="' + $inputs[i].name + '"]');
                            $dataInput.each(function () {
                                if(this.tagName.toUpperCase() === 'INPUT'){
                                    $(this).val($inputs[i].value)
                                } else {
                                    $(this).text($inputs[i].value)
                                }
                            });
                        }
                    },
                    shipping_card :{
                        /** @param { { mass, mass_total, shipping : { } } } order **/
                        setShippingCardValue(order) {
                            this.clearAddresses();
                            this.setMass(order.mass, order.mass_total);

                            let $form = $logistic.find('[data-id="shipping_address"].hide');
                            for (let address of order.shipping) {
                                this.addNewAddress($form, address)
                            }
                        },
                        setMass (unit, total) {
                            $logistic.find('[data-input="mass"]').text(parseFloat(unit).toFixed(2));
                            $logistic.find('[data-input="mass_total"]').text(total);
                        },
                        addNewAddress($html, address) {
                            let $clone = $html.clone(),
                                prev_id = $html.prev('[data-id="shipping_address"]').attr('data-count'),
                                current_id = parseInt(typeof prev_id !== "undefined" ? prev_id : '0') + 1
                            ;

                            $clone.attr('data-count', current_id);

                            $clone.find("[data-input]").each((index, label) => {
                                let $label = $(label),
                                    input = $label.attr('data-input'),
                                    text = address[input];
                                if ($label.attr('data-enter') && text) {
                                    text = text.split('\n').map( (item) => `${item}<br/>` );
                                }
                                $label.html(text);
                            });

                            $html.before($clone.removeClass('hide'));
                        },
                        clearAddresses() {
                            $logistic.find('[data-id="shipping_address"]:not(.hide)').remove();
                        },
                    },
                    makeTabsActive() {
                        $('.nav li.disabled').removeClass('disabled');
                    }
                },
                language : {
                    languageArray : {},
                    addLanguageOption() {
                        Object.keys(languages).forEach(function(item){
                            $languageSelect.append($('<option>', {
                                value: item,
                                text : item.toUpperCase()
                            }));
                            }
                        );
                    },

                    setLanguageArray(value) {
                        let language = languages[value];
                        if (typeof language === 'undefined') {
                            this.languageArray = {};
                        } else {
                            this.languageArray = language;
                        }
                    },

                    changeLanguage(){
                        let langText = document.querySelectorAll('[data-language]');
                        for (let i = 0 ; i < langText.length ; i++ ){
                            let element = langText[i].getAttribute('data-language');
                            if (typeof this.languageArray[element] === 'undefined'){
                                langText[i].innerHTML = element;
                            } else {
                                langText[i].innerHTML = this.languageArray[element];
                            }
                        }
                    },
                    init : function () {
                        this.setLanguageArray(locale);
                        this.changeLanguage();
                    }
                },

                email : {
                    get(operator_email){
                        if (operator_email){
                            $operatorMail.val(operator_email)
                        }
                    }
                }
            };

        f.language.init();

        /**
         * Ustawia startowo taba na #home
         */
        $('a[href="#home"]').click();

        /**
         * dodawanie inputa do selectiza z paletami
         */
       $('.selectizer').selectize({
           create: true,
           sortField: 'text',
           render: {
               option_create: function(data, escape) {
                   return '<div class="create">{{ __('Add') }} <strong>' + escape(data.input) + '</strong>&hellip;</div>';
               }
           }
       });

        /**
         * Zarejestrowanie kliknięcia drukowania.
         */
        let afterPrint = function() {
            $('button#action').attr('disabled', false);
        };
        if (window.matchMedia) {
            let mediaQueryList = window.matchMedia('print');
            function checkMatches(mql) {
                if (!mql.matches) {
                    afterPrint();
                }
            }
            mediaQueryList.addListener(checkMatches);
        }
        window.onafterprint = afterPrint;

        /**
         * Wydrukowanie strony.
         */
        $(document).on('click', '#print-button #print', function () {
            $($.admin.options.layout.contentWrap).css('min-height', 300);
            return window.print();
        });

        /**
         * Ustawienia przycisku drukowania
         */
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            let target = $(e.target).attr("href");
            let printButton = $('#print');
            let pdfButton = $('#pdf');
            let emailButton = $('#email');
            emailButton.attr('disabled', true).addClass('disabled');
            if (target !== "#home"){
                printButton.attr('disabled', false).removeClass('disabled');
                pdfButton.attr('disabled', false).removeClass('disabled');
                $languageSelect.attr('disabled', false).removeClass('disabled');

                f.labels.setDataInForm(e);
                f.labels.addAddressSelect(f.labels.getFormAddresses());
                f.labels.barcode.displayCode($('#isbn').val());

                if (target === "#palette_label"){
                    $('#label-generator').attr('action', '{{ route('label-generator.pdf.palette') }}')
                } else if(target === "#delivery_note") {
                    $('#label-generator').attr('action', '{{ route('label-generator.pdf.delivery') }}');
                    if($operatorMail.val() !== ''){
                        emailButton.attr('disabled', false).removeClass('disabled');
                    } else {
                        emailButton.attr('disabled', true).addClass('disabled');
                    }
                } else {
                    pdfButton.attr('disabled', true).addClass('disabled');
                }
            } else {
                printButton.attr('disabled', true).addClass('disabled');
                pdfButton.attr('disabled', true).addClass('disabled');
                $languageSelect.attr('disabled', true).addClass('disabled');
                f.labels.barcode.show();
            }
        });

        /**
         * Pobieramy dane zamówienia z Optimy
         */
        $('#order_number + span .btn').on('click', function() {
            let $formGroup = $(this).parents('.form-group'),
                order_number = $('#order_number').val().replace(new RegExp('/', 'g'), '_');
            $.admin.ajax.get({
                url: '{{ route('api.mis.info') }}/'+order_number,
                loadingState: $formGroup
            }, '{{ auth()->user()->getApiToken() }}').done(function(response){
                f.labels.makeTabsActive();
                f.labels.clearMainForm();
                f.labels.clearAddressForm();
                Object.keys(response.data).forEach(function(key){
                    $('#'+key).val(response.data[key]);
                });
                f.labels.getShippingAddress(response);
                f.labels.getCustomerAddress(response);
                f.labels.shipping_card.setShippingCardValue(response.data);
                f.labels.barcode.displayCode(response.data.isbn);
                f.email.get(response.data.operator_email);
                $('.nav li.disabled').removeClass('disabled');
            }).fail(function () {
                f.labels.clearMainForm();
                f.labels.clearAddressForm();
            });
        });

        /**
         * Dodajemy nowy formularz na adres
         */
        $('#add-address-button').on('click', function (e) {
            e.preventDefault();
            f.labels.addNewAddressForm();
        });

        $addressSelect.on('change', function () {
            let id = $(this).find('option:selected').val(),
                $tab = $(this).parents('.tab-pane'),
                $addressForm = $formBox.find(addressPanel+'[data-id="'+id+'"]');

            $tab.find('input[data-input="shipping_name"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[name]']").val());
            $tab.find('input[data-input="shipping_address"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[address]']").val());
            $tab.find('input[data-input="shipping_city"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[city]']").val());
            $tab.find('input[data-input="shipping_post_code"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[postal_code]']").val());
            $tab.find('input[data-input="shipping_country"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[country]']").val());
            $tab.find('input[data-input="telephone"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[phone_number]']").val());
            $tab.find('input[data-input="fax"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[fax]']").val());
            $tab.find('input[data-input="email"]').val($addressForm.find("input[name='shipping\\[" + id + "]\\[email]']").val());

            $tab.find('input[data-input="customer_name"]').val()
        });

        $languageSelect.on('change', function () {
            let language = $(this).find('option:selected').val();
            f.language.setLanguageArray(language);
            f.language.changeLanguage();
        });

        /**
         *  Usuwanie kodu kreskowego.
         */
        $barcode.next('.close').click(function() {
            f.labels.barcode.hide();
        });
    });
</script>
@endpush
@push('styles')
    <style>
        .nav li.disabled {
            pointer-events: none;
        }
        @media print {
            .print-no-border {
                border: 0;
            }

            .print-zoom {
                zoom: 1.4;
            }
            .print-zoom-out {
                zoom: 0.8;
            }

            #delivery .selectize-input {
                border: 0;
            }

            #delivery .input-group-addon {
                border: 0;
                display: none;
            }

            .print-no-display {
                display: none;
            }

            .selectize-control.single .selectize-input:after{
                display: none;
            }

            .form-group {
                margin-bottom: 0;
            }

            .print-height-auto {
                height: auto;
            }
        }
    </style>
@endpush

