<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .form-group {
            margin-bottom: 0;
        }
        .print-zoom {
            zoom: 1.4;
        }
    </style>
</head>
<body>
<div class="box-body print-zoom form-horizontal">
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Publisher')}}</div>
                    <div class="col-xs-8">
                        <span>{{$customer}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Title')}}</div>
                    <div class="col-xs-8">
                        <span>{{$title}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">ISBN</div>
                    <div class="col-xs-8">
                        <span>{{$isbn}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Retail price')}}</div>
                    <div class="col-xs-8">
                        <span>{{$price}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Number of cartons')}}</div>
                    <div class="col-xs-8">
                        <span>{{$cartons_number}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Total quantity')}}</div>
                    <div class="col-xs-8">
                        <span>{{$total_quantity}}</span>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-6 control-label text-uppercase">
                        <b>{{__('Address of delivery')}}</b>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Name')}}</div>
                    <div class="col-xs-7">
                        <span>{{$palette_shipping_name}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Address')}}</div>
                    <div class="col-xs-7">
                        <span>{{$palette_shipping_address}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('City')}}</div>
                    <div class="col-xs-7">
                        <span>{{$palette_shipping_city}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Postal code')}}</div>
                    <div class="col-xs-7">
                        <span>{{$palette_shipping_post_code}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase">{{__('Country')}}</div>
                    <div class="col-xs-7">
                        <span>{{$palette_shipping_country}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <br>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase"><b>Tel:</b></div>
                    <div class="col-xs-7">
                        <span>{{$palette_telephone}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase"><b>Fax:</b></div>
                    <div class="col-xs-7">
                        <span>{{$palette_fax}}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-2 col-xs-10">
                <div class="form-group">
                    <div class="col-xs-4 control-label text-uppercase"><b>Email:</b></div>
                    <div class="col-xs-7">
                        <span>{{$palette_email}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>