<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .form-group {
            margin-bottom: 10px;
        }
        .print-zoom {
             zoom: 1.4;
         }
    </style>
</head>
<body>
    <div id="delivery" class="box-body form-horizontal print-zoom">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <b>{{__('Customer/Publisher:')}}</b>
                    </div>
                    <div class="form-group">
                        <span>{{$customer_name}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$customer_address}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$customer_city}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$customer_post_code}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$customer_country}}</span>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <b>{{__('Shipping Address:')}}</b>
                    </div>
                    <div class="form-group">
                        <span>{{$shipping_name}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$shipping_address}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$shipping_city}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$shipping_post_code}}</span>
                    </div>
                    <div class="form-group">
                        <span>{{$shipping_country}}</span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group"></div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-9 col-xs-offset-3 text-uppercase">
                                <b>{{__('Delivery note')}}</b>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Publisher')}}</div>
                            <div class="col-xs-8">
                                <span>{{$customer}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Title')}}</div>
                            <div class="col-xs-8">
                                <span>{{$order_name}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">ISBN</div>
                            <div class="col-xs-8">
                                <span>{{$isbn}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Number of copies per carton')}}</div>
                            <div class="col-xs-8">
                                <span>{{$copies_number}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Total number of cartons')}}</div>
                            <div class="col-xs-8">
                                <span>{{$cartons_number}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Total number of copies')}}</div>
                            <div class="col-xs-8">
                                <span>{{$total_copies}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Total number of pallets')}}</div>
                            <div class="col-xs-8">
                                <span>{{$total_pallets}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Palette dimensions')}}</div>
                            <div class="col-xs-8">
                                <span>{{$palette_dimensions}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Net weight')}}</div>
                            <div class="col-xs-8">
                                <span>{{$net_weight}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Gross weight')}}</div>
                            <div class="col-xs-8">
                                <span>{{$gross_weight}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label">{{__('Loading/dispatch date')}}</div>
                            <div class="col-xs-8">
                                <span>{{$date}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-1 col-xs-11">
                            <div class="col-xs-4 control-label"><b>{{__('Country of origin')}}</b></div>
                            <div class="col-xs-8">
                                <span>{{__('Poland')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group">
                        <div class="col-xs-9 col-xs-offset-2">
                            <b>{{__('Please return one signed copy to the driver')}}</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>