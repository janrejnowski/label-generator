<div id="main-form" class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label for="order_number" class="col-xs-3 control-label">{{__('Order number')}}</label>
            <div class="col-xs-7">
                <div class="input-group">
                    <input type="text" class="form-control" id="order_number" name="order_number" placeholder="{{__('Order number')}}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success">{{__('Download')}}</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="isbn" class="col-xs-3 control-label">ISBN</label>
            <div class="col-xs-7">
                <input type="text" class="form-control" id="isbn" name="isbn" placeholder="ISBN">
            </div>
        </div>
        <div class="form-group">
            <label for="order_name" class="col-xs-3 control-label">{{__('Title')}}</label>
            <div class="col-xs-7">
                <input type="text" class="form-control" id="order_name" name="order_name" placeholder="{{__('Title')}}">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label for="author" class="col-xs-3 control-label">{{__('Author')}}</label>
            <div class="col-xs-7">
                <input type="text" class="form-control" id="author" name="author" placeholder="{{__('Author')}}">
            </div>
        </div>
        <div class="form-group">
            <label for="weight" class="col-xs-3 control-label">{{__('Weight')}}</label>
            <div class="col-xs-7">
                <div class="input-group">
                    <input type="text" class="form-control" id="weight" name="weight" placeholder="{{__('Weight')}}">
                    <span class="input-group-addon">{{ __('kgs') }}</span>
                </div>
            </div>
        </div>
        <div class="form-group condition">
            <label for="price" class="col-xs-3 control-label">{{__('Price')}}</label>
            <div class="col-xs-7">
                <div class="input-group">
                    <input type="text" class="form-control" id="price" name="price" placeholder="{{__('Price')}}">
                    <div class="input-group-btn">
                        <select class="btn btn-default dropdown-toggle" id="price_currency" name="price_currency" title="{{ __('Choose currency') }}">
                            <option value="PLN">PLN</option>
                            <option value="GBP">GBP</option>
                            <option value="EUR">EUR</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="copies_quantity" class="col-xs-3 control-label">{{__('Copies quantity')}}</label>
            <div class="col-xs-7">
                <div class="input-group">
                    <input type="text" class="form-control" id="copies_quantity" name="copies_quantity" placeholder="{{__('Copies quantity')}}">
                    <span class="input-group-addon">{{ __('cps') }}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="dimensions" class="col-xs-3 control-label">{{__('Carton dimensions')}}</label>
            <div class="col-xs-7">
                <div class="input-group">
                    <select class="form-control selectizer" id="dimensions" name="dimensions">
                        <option value="225 x 300 x 360">225 x 300 x 360</option>
                        <option value="245 x 370 x 385">245 x 370 x 385</option>
                        <option value="290 x 400 x 410">290 x 400 x 410</option>
                    </select>
                    <span class="input-group-addon">{{ __('mm') }}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
        <input type="hidden" id="pdf_email" name="pdf_email" value="">
        </div>
    </div>
</div>