<p><b>{{__('Shipping address')}}</b></p>
<div id="address-box" class="row">
    <div id="value-box">
        <div class="col-xs-6 hide address-box">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="shipping_name[0]" class="col-xs-4 control-label">{{__('Name')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="shipping_name[0]" name="shipping_name[0]" placeholder="{{__('Name')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shipping_address[0]" class="col-xs-4 control-label">{{__('Address')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="shipping_address[0]" name="shipping_address[0]" placeholder="{{__('Address')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shipping_city[0]" class="col-xs-4 control-label">{{__('City')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="shipping_city[0]" name="shipping_city[0]" placeholder="{{__('City')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shipping_post_code[0]" class="col-xs-4 control-label">{{__('Post code')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="shipping_post_code[0]" name="shipping_post_code[0]" placeholder="{{__('Post code')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shipping_country[0]" class="col-xs-4 control-label">{{__('Country')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="shipping_country[0]" name="shipping_country[0]" placeholder="{{__('Country')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telephone[0]" class="col-xs-4 control-label">{{__('Telephone')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="telephone[0]" name="telephone[0]" placeholder="{{__('Telephone')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fax[0]" class="col-xs-4 control-label">{{__('Fax')}}</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="fax[0]" name="fax[0]" placeholder="{{__('Fax')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email[0]" class="col-xs-4 control-label">Email</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="email[0]" name="email[0]" placeholder="Email">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 text-center">
        <h3><b>{{__('Add new address')}}</b></h3>
        <br>
        <div>
            <button id="add-address-button" type="button" class="btn btn-lg btn-success"><i class="fa fa-plus"></i></button>
        </div>
    </div>
</div>
<hr>

<p><b>{{__('Customer/Publisher address')}}</b></p>
<div id="customer-box" class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="form-group">
                    <label for="customer[name]" class="col-xs-3 control-label">{{__('Name')}}</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="customer[name]" name="customer[name]" placeholder="{{__('Name')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer[address]" class="col-xs-3 control-label">{{__('Address')}}</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="customer[address]" name="customer[address]" placeholder="{{__('Address')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer[city]" class="col-xs-3 control-label">{{__('City')}}</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="customer[city]" name="customer[city]" placeholder="{{__('City')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer[post_code]" class="col-xs-3 control-label">{{__('Post code')}}</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="customer[post_code]" name="customer[post_code]" placeholder="{{__('Post code')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer[country]" class="col-xs-3 control-label">{{__('Country')}}</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="customer[country]" name="customer[country]" placeholder="{{__('Country')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

