<?php

namespace JanRejnowski\LabelGenerator;

use Illuminate\View\View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class LabelServiceProvider extends ServiceProvider
{
    public const NAMESPACE = 'label-generator';
    public const LANG = __DIR__ . '/resources/lang';

    public function boot() : void
    {
        $this->configure();

        $this->loadViewsFrom(__DIR__ . '/resources/views', self::NAMESPACE);
        $this->loadJsonTranslationsFrom(self::LANG);

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path(self::NAMESPACE.'.php'),
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/'.self::NAMESPACE),
        ]);
        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/'.self::NAMESPACE),
        ], self::NAMESPACE.'-views');
        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ], self::NAMESPACE.'-migrations');
    }

    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__.'/config/config.php', self::NAMESPACE);

        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->app->bind(self::class, function() {
            return $this;
        });
    }

    private function configure() : void
    {
        view()->composer('layout.home', function (View $view) {
            $this->extendMenu($this->makeMenu($view), $view);
        });
    }

    private function makeMenu(View $view) : array
    {
        try {
            return make_menu(config(self::NAMESPACE.'.menu'), $view->offsetGet('currentUser'));
        } catch (\Error $e) {
            return [];
        }
    }

    private function extendMenu(array $data, View $view) : void
    {
        if (!empty($data)) {
            if ($view->offsetGet('menu')->contains('route', $data[0]->route)) {
                $view->offsetGet('menu')->map(function ($item) use ($data) {
                    if ($item->route === $data[0]->route) {
                        $item->children = array_merge($item->children, $data[0]->children);
                    }
                    return $item;
                });
            } else {
                $view->offsetSet('menu', $view->offsetGet('menu')->merge(collect($data)));
            }
        }
    }

    public function getLanguagesArray() : array
    {
        $files = File::glob(self::LANG.'/*.json');
        $result = [];
        array_walk($files, function(&$filename) use (&$result) {
            $result[strstr(basename($filename), '.json', true)] = json_decode(file_get_contents($filename));
        });
        return $result;
    }

    public static function setPdfName(string $name, string $orderNumber = null) : string
    {
        $checkedName = preg_replace('/\s+/', '_',$name);
        if ($orderNumber){
            return str_replace('/','_',$orderNumber).'-'.$checkedName.'.pdf';
        }
        return $checkedName.'.pdf';
    }
}
