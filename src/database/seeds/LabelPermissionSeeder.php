<?php

namespace JanRejnowski\LabelGenerator\Database\Seeds;

use Illuminate\Database\PermissionsContractSeeder;

class LabelPermissionSeeder extends PermissionsContractSeeder
{
    public function permissions() : array
    {
        return [
            [
                'name' => 'label-generator',
                'description' => 'Create shipping labels'
            ]
        ];
    }
}
