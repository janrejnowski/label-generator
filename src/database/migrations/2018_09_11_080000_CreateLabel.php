<?php

use JanRejnowski\LabelGenerator\Database\Seeds\LabelPermissionSeeder;
use Illuminate\Database\Migrations\Migration;

class CreateLabel extends Migration
{

    public function up() : void
    {
        try{
            \Illuminate\Support\Facades\Artisan::call('db:seed', [
                '--class' => LabelPermissionSeeder::class
            ]);

        } catch (PDOException $e){
            $this->down();
            throw $e;
        }
    }

    public function down() : void
    {
        $labelPermission = new LabelPermissionSeeder();
        $labelPermission->down();
    }
}
