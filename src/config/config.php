<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu item
    |--------------------------------------------------------------------------
    |
    | This is the array of menu item in sidebar of system.
    |
    */
    'menu' => [
        [
            'key' => 'job-ticket|delivery-statement|label-generator',
            'name' => 'Orders service',
            'route' => 'orders',
            'route-prefix' => 'orders',
            'icon-class' => 'fa-ticket',
            'children' => [
                [
                    'key' => 'label-generator',
                    'name' => 'Label Generator',
                    'route' => 'label-generator.index',
                    'route-prefix' => 'label',
                    'icon-class' => 'fa-sticky-note',
                    'children' => [],
                ],
            ],
        ],
    ],

];