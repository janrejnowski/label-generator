<?php

namespace JanRejnowski\LabelGenerator\Notifications;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Notifications\Messages\MailMessage;
use JanRejnowski\LabelGenerator\LabelServiceProvider;
use Illuminate\Notifications\Notification;

class PdfCreated extends Notification
{
    use Queueable;

    protected $pdf;
    protected $request;

    public function __construct(PdfWrapper $pdf, Request $request)
    {
        $this->pdf = $pdf;
        $this->request = $request;
    }

    public function via() : array
    {
        return ['mail'];
    }

    public function toMail() : MailMessage
    {
        return (new MailMessage)
            ->subject(__('Delivery Note for Order: :order_number', ['order_number' => $this->request->order_number]))
            ->greeting( __('Hello!'))
            ->line( __('Delivery Note for Order: :order_number', ['order_number' => $this->request->order_number]) )
            ->line( __('Order Title: :order_name', ['order_name' => $this->request->order_name]) )
            ->line( __('Customer: :customer', ['customer' => $this->request->input('customer.name')]) )
            ->line( '' )
            ->attachData($this->pdf->output(), LabelServiceProvider::setPdfName('delivery-note', $this->request->order_number));
    }
}
